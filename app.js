/* global $, vis, console */
"use strict";

// Config
numeral.language("pt-br");

// Vértices
var nodes = [
    {id: 0, status: 0, d:Infinity, prev:undefined, label: "AC (0)", name: "Acre", x:-317, y:-83}, // 0
    {id: 1, status: 0, d:Infinity, prev:undefined, label: "AL (1)", name: "Alagoas", x:311, y:-77}, // 1
    {id: 2, status: 0, d:Infinity, prev:undefined, label: "AP (2)", name: "Amapa", x:3, y:-291}, // 2
    {id: 3, status: 0, d:Infinity, prev:undefined, label: "AM (3)", name: "Amazonas", x:-230, y:-196}, // 3
    {id: 4, status: 0, d:Infinity, prev:undefined, label: "BA (4)", name: "Bahia", x:185, y:-30}, // 4
    {id: 5, status: 0, d:Infinity, prev:undefined, label: "CE (5)", name: "Ceará", x:225, y:-176}, // 5
    {id: 6, status: 0, d:Infinity, prev:undefined, label: "DF (6)", name: "Distrito Federal", x:75, y:29}, // 6
    {id: 7, status: 0, d:Infinity, prev:undefined, label: "ES (7)", name: "Espírito Santo", x:210, y:100}, // 7
    {id: 8, status: 0, d:Infinity, prev:undefined, label: "GO (8)", name: "Goiás", x:23, y:51}, // 8
    {id: 9, status: 0, d:Infinity, prev:undefined, label: "MA (9)", name: "Maranhão", x:118, y:-173}, // 9
    {id: 10, status: 0, d:Infinity, prev:undefined, label: "MT (10)", name: "Mato Grosso", x:-82, y:-16}, // 10
    {id: 11, status: 0, d:Infinity, prev:undefined, label: "MS (11)", name: "Mato Grosso do Sul", x:-71, y:110}, // 11
    {id: 12, status: 0, d:Infinity, prev:undefined, label: "MG (12)", name: "Minas Gerais", x:137, y:76}, // 12
    {id: 13, status: 0, d:Infinity, prev:undefined, label: "PA (13)", name: "Pará", x:-18, y:-179}, // 13
    {id: 14, status: 0, d:Infinity, prev:undefined, label: "PB (14)", name: "Paraíba", x:332, y:-146}, // 14
    {id: 15, status: 0, d:Infinity, prev:undefined, label: "PR (15)", name: "Paraná", x:3, y:194}, // 15
    {id: 16, status: 0, d:Infinity, prev:undefined, label: "PE (16)", name: "Pernambuco", x:326, y:-109}, // 16
    {id: 17, status: 0, d:Infinity, prev:undefined, label: "PI (17)", name: "Piauí", x:170, y:-120}, // 17
    {id: 18, status: 0, d:Infinity, prev:undefined, label: "RJ (18)", name: "Rio de Janeiro", x:173, y:151}, // 18
    {id: 19, status: 0, d:Infinity, prev:undefined, label: "RN (19)", name: "Rio Grande do Norte", x:299, y:-183}, // 19
    {id: 20, status: 0, d:Infinity, prev:undefined, label: "RS (20)", name: "Rio Grande do Sul", x:-17, y:288}, // 20
    {id: 21, status: 0, d:Infinity, prev:undefined, label: "RO (21)", name: "Rondônia", x:-197, y:-51}, // 21
    {id: 22, status: 0, d:Infinity, prev:undefined, label: "RR (22)", name: "Roraima", x:-170, y:-308}, // 22
    {id: 23, status: 0, d:Infinity, prev:undefined, label: "SC (23)", name: "Santa Catarina", x:42, y:247}, // 23
    {id: 24, status: 0, d:Infinity, prev:undefined, label: "SP (24)", name: "São Paulo", x:63, y:151}, // 24
    {id: 25, status: 0, d:Infinity, prev:undefined, label: "SE (25)", name: "Sergipe", x:283, y:-47}, // 25
    {id: 26, status: 0, d:Infinity, prev:undefined, label: "TO (26)", name: "Tocantins", x:65, y:-65} // 26
];

// Arestas
var edges = [
    {id:1, from: 0, to: 3, cost: 1240},{from: 3, to: 0, cost: 100},
    {from: 0, to: 21, cost: 25},{from: 21, to: 0, cost: 874},

    {from: 1, to: 4, cost: 874},{from: 4, to: 1, cost: 50},

    {from: 2, to: 3, cost: 50},{from: 3, to: 2, cost: 25},
    {from: 2, to: 9, cost: 874},{from: 9, to: 2, cost: 640},
    {from: 2, to: 13, cost: 1240},{from: 13, to: 2, cost: 640},
    {from: 2, to: 22, cost: 15},{from: 22, to: 2, cost: 200},

    {from: 3, to: 10, cost: 200},{from: 10, to: 3, cost: 640},
    {from: 3, to: 13, cost: 874},{from: 13, to: 3, cost: 25},
    {from: 3, to: 21, cost: 1240},{from: 21, to: 3, cost: 640},
    {from: 3, to: 22, cost: 200},{from: 22, to: 3, cost: 25},

    {from: 4, to: 5, cost: 640},{from: 5, to: 4, cost: 640},
    {from: 4, to: 7, cost: 1240},{from: 7, to: 4, cost: 640},
    {from: 4, to: 12, cost: 874},{from: 12, to: 4, cost: 1240},
    {from: 4, to: 14, cost: 150},{from: 14, to: 4, cost: 25},
    {from: 4, to: 16, cost: 50},{from: 16, to: 4, cost: 100},
    {from: 4, to: 17, cost: 200},{from: 17, to: 4, cost: 1240},
    {from: 4, to: 19, cost: 50},{from: 19, to: 4, cost: 100},
    {from: 4, to: 25, cost: 874},{from: 25, to: 4, cost: 1240},
    {from: 4, to: 26, cost: 640},{from: 26, to: 4, cost: 150},

    {from: 6, to: 8, cost: 640},{from: 8, to: 6, cost: 200},
    {from: 6, to: 10, cost: 1240},{from: 10, to: 6, cost: 874},
    {from: 6, to: 12, cost: 1240},{from: 12, to: 6, cost: 150},
    {from: 6, to: 26, cost: 100},{from: 26, to: 6, cost: 25},

    {from: 7, to: 18, cost: 15},{from: 18, to: 7, cost: 640},
    {from: 7, to: 24, cost: 200},{from: 24, to: 7, cost: 50},
    {from: 7, to: 25, cost: 874},{from: 25, to: 7, cost: 150},

    {from: 8, to: 10, cost: 640},{from: 10, to: 8, cost: 50},
    {from: 8, to: 24, cost: 1240},{from: 24, to: 8, cost: 200},

    {from: 9, to: 13, cost: 874},{from: 13, to: 9, cost: 25},
    {from: 9, to: 17, cost: 1240},{from: 17, to: 9, cost: 150},
    {from: 9, to: 26, cost: 874},{from: 26, to: 9, cost: 25},

    {from: 10, to: 11, cost: 100},{from: 11, to: 10, cost: 15},
    {from: 10, to: 13, cost: 50},{from: 13, to: 10, cost: 1240},
    {from: 10, to: 21, cost: 200},{from: 21, to: 10, cost: 1240},
    {from: 10, to: 26, cost: 150},{from: 26, to: 10, cost: 25},

    {from: 11, to: 15, cost: 50},{from: 15, to: 11, cost: 50},
    {from: 11, to: 24, cost: 1240},{from: 24, to: 11, cost: 874},

    {from: 12, to: 24, cost: 1240},{from: 24, to: 12, cost: 200},

    {from: 15, to: 23, cost: 874},{from: 23, to: 15, cost: 50},
    {from: 15, to: 24, cost: 874},{from: 24, to: 15, cost: 25},

    {from: 18, to: 23, cost: 874},{from: 23, to: 18, cost: 1240},
    {from: 18, to: 24, cost: 1240},{from: 24, to: 18, cost: 50},

    {from: 20, to: 23, cost: 50},{from: 23, to: 20, cost: 874}
];

// Para buscar o índice (ID) da próxima aresta da "fila"
function getNextNodeIndex() {
    var next = null;
    var current = null;
    var lastIndex;
    for (var i in nodes) {
        current = nodes[i];
        if (current.status === 1) {
            if (next === null || current.d < next.d) {
                next = current;
                lastIndex = i;
            }
        }
    }
    return parseInt(lastIndex);
}

// Para buscar a lista de arestas adjacentes de um vértice
function getEdgesForNodeId(nodeId) {
    var arr = [];
    var current = null;
    for (var i in edges) {
        current = edges[i];
        if (current.from === nodeId) {
            arr.push(current);
        }
    }
    return arr;
}

// Para fazer o cálculo do caminho mínimo
function dijkstra(idStart, idEnd) {
    var firstNode = nodes[idStart]; // Primeiro vértice
    var lastNode = nodes[idEnd]; // Último vértice
    firstNode.status = 1; // Coloca o primeiro vértice na "fila"
    firstNode.d = 0; // Distância do primeiro vértice precisa ser zero
    // Roda enquanto não encontrar o melhor caminho até o node final
    while (lastNode.status !== 2) {
        var nextIndex = getNextNodeIndex(); // ID do próximo vértice
        var adjs = getEdgesForNodeId(nextIndex); // Arestas adjacentes
        var w = nodes[nextIndex]; // Vértice atual
        w.status = 2; // Marca como visitado
        for (var i = 0; i < adjs.length; i += 1) {
            var currentAdj = nodes[adjs[i].to]; // Vértice adjacente
            // não pode ser visitado
            if (currentAdj.status !== 2) {
                currentAdj.status = 1;
                var edgeValue = adjs[i].cost || 1;
                var alt = w.d + edgeValue;
                if (alt < currentAdj.d) {
                    currentAdj.d = alt;
                    currentAdj.prev = w;
                }
            }
        }
    }
    return lastNode;
}

// Imprime o caminho em forma de lista e no gráfico
function printPath(lastNode) {
    var stack = [];
    var nodesToSelect = [];
    var nodeToPush = lastNode;
    var popedNode = null;
    var result = "";
    while (nodeToPush !== undefined) {
        stack.push(nodeToPush);
        nodesToSelect.push(nodeToPush.id);
        nodeToPush = nodeToPush.prev;
    }
    popedNode = stack.pop();
    while (popedNode) {
        result += "<li>" + popedNode.name + " - " + numeral(popedNode.d).format("$0,0.00") + "</li>";
        popedNode = stack.pop();
    }
    graph.selectNodes(nodesToSelect);
    $("#path").html(result);
}

// Precisa capturar o envio do formulário para criar o caminho entre os pontos
$("#form").on("submit", function (event) {
    event.preventDefault();
    var de = $("#de").val();
    var para = $("#para").val();
    var lastNode = dijkstra(de, para);
    printPath(lastNode)
});

// GRAPH (library configuration)
// create a graph
var container = document.getElementById("graph");
var data= {
    nodes: nodes,
    edges: edges,
};
var options = {
    width: "845px",
    height: "744px",
    zoomable: false,
    dragNodes: true,
    dragGraph: false,
    smoothCurves: false,
    selectable: true,
    physics: {
        barnesHut: {
            enabled: true,
            gravitationalConstant: 0,
            centralGravity: 0,
            springLength: 0,
            springConstant: 0,
            damping: 0
        }
    },
    nodes: {
        allowedToMoveX: false,
        allowedToMoveY: false,
        fontSize:10
    },
    edges: {
        fontFill: "transparent",
        style:"line",
        width: 2
    }
};

// Create the graph
var graph = new vis.Network(container, data, options);
// Reset the graph's position
graph.moveTo({
    position:{x:0, y:0},
    offset:{x:0,y:0}
});